import sys
from tkinter import *
from math import *

ventana = Tk()
ventana.title ("Calculadora")
color_boton=("gray77")


frame = Frame(ventana)
frame.grid(column=0, row=0, padx=(50,50), pady=(50,50))
frame.columnconfigure(0, weight=1)
frame.rowconfigure(0, weight=1)

texto=StringVar()
valor = "" 
pantalla = Entry(frame,font=('arial',20,'bold'),width=25, textvariable=texto, justify="right")
pantalla.grid(column=1, row=1, columnspan=5)


class Longitudes:
    def __init__(self):
        self.texto=StringVar()
        self.valor = ""
    def pulgadas(self):
        valor = float(pantalla.get())
        valor2 = 25.4
        conversion = valor*valor2
        return texto.set(conversion)

    def centimetros(self):
        valor=float(pantalla.get())
        valor2= 10
        conversion =valor/valor2
        return texto.set(conversion)

    def metros(self):
        valor = float(pantalla.get())
        valor2 = 100
        conversion= valor/valor2
        return texto.set(conversion)

    def kilometros(self):
        valor = float(pantalla.get())
        valor2 = 1000
        conversion= valor/valor2
        return texto.set(conversion)


class Volumenes:
    def __init__(self):
        self.texto = StringVar()
        self.valor = ""
    
    def cm3m3(self):
    
        valor= float(pantalla.get())
        valor2= 1000000
        conversion= valor/valor2
        return texto.set(conversion)

    def m3l(self):
    
        valor= float(pantalla.get())
        valor2= 0.001
        conversion= valor/valor2
        return texto.set(conversion)

    def lgl(self):
    
        valor= float(pantalla.get())
        valor2= 3.78541
        conversion= valor/valor2
        return texto.set(conversion) 

    def glcm3(self):
    
        valor= float(pantalla.get())
        valor2= 0.000264172
        conversion= valor/valor2
        return texto.set(conversion)  

class Presion:
    def __init__(self):
        self.texto = StringVar()
        self.valor = ""

    def pascalatm(self):
        valor = float(pantalla.get())
        valor2 = 101325
        conversion= valor/valor2
        return texto.set(conversion)

    def atmtorr(self):
        valor = float(pantalla.get())
        valor2 = 0.00131579
        conversion= valor/valor2
        return texto.set(conversion)

    def torrpascal(self):
        valor = float(pantalla.get())
        valor2 = 0.00750062
        conversion= valor/valor2
        return texto.set(conversion)

class Operaciones:

    def __init__(self):
        self.texto = StringVar()
        self.valor = ""


    def clear(self):
        global valor
        valor=("")
        texto.set("0")

    def operacion(self):
        global valor
        try:
            opera=str(eval(valor))
        except:
            clear()
            opera=("ERROR")
        texto.set(opera)

class Peso:
    def __init__(self):
        self.texto = StringVar()
        self.valor = ""

    def grkil(self):
        valor=float(pantalla.get())
        valor2=1000
        conversion=valor/valor2
        return texto.set(conversion)

    def kilton(self):
        valor=float(pantalla.get())
        valor2=1000
        conversion=valor/valor2
        return texto.set(conversion)

    def tonlib(self):
        valor=float(pantalla.get())
        valor2=2204.62
        conversion=valor*valor2
        return texto.set(conversion)

def clik(num):
    global valor
    valor = valor + str(num)
    texto.set(valor)


S=Longitudes()
V=Volumenes()
P=Presion()
O=Operaciones()
Pe=Peso()

b7 = Button(frame, text ="7", width=2, command=lambda:clik(7))
b7.grid(column=1, row=2)

b8 = Button(frame, text ="8", width=2, command=lambda:clik(8))
b8.grid(column=2, row=2)

b9 = Button(frame, text ="9", width=2, command=lambda:clik(9))
b9.grid(column=3, row=2)

dividir = Button(frame, text ="/", width=2, command=lambda:clik("/"))
dividir.grid(column=4, row=2)

elevar = Button(frame, text ="Elevar", width=4, command=lambda:clik("**"))
elevar.grid(column=5, row=2)

ln= Button(frame,text="ln",width=8,command=lambda:clik("log"))
ln.grid(column=6,row=3)

b4 = Button(frame, text ="4", width=2, command=lambda:clik(4))
b4.grid(column=1, row=3)

b5 = Button(frame, text ="5", width=2, command=lambda:clik(5))
b5.grid(column=2, row=3)

b6 = Button(frame, text ="6", width=2, command=lambda:clik(6))
b6.grid(column=3, row=3)

multi = Button(frame, text ="*", width=2, command=lambda:clik("*"))
multi.grid(column=4, row=3)

raiz = Button(frame, text ="√", width=4, command=lambda:clik("sqrt"))
raiz.grid(column=5, row=3)

pi = Button(frame, text ="π", width=4, command=lambda:clik("pi"))
pi.grid(column=5, row=4)

euler = Button(frame, text ="e", width=4, command=lambda:clik("e"))
euler.grid(column=5, row=5)

b1 = Button(frame, text ="1", width=2, command=lambda:clik(1))
b1.grid(column=1, row=4)

b2 = Button(frame, text ="2", width=2, command=lambda:clik(2))
b2.grid(column=2, row=4)

b3 = Button(frame, text ="3", width=2, command=lambda:clik(3))
b3.grid(column=3, row=4)

restar = Button(frame, text ="-", width=2, command=lambda:clik("-"))
restar.grid(column=4, row=4)

b0 = Button(frame, text ="0", width=6, command=lambda:clik(0))
b0.grid(column=1, row=5, columnspan=2)

deci = Button(frame, text =".", width=2, command=lambda:clik("."))
deci.grid(column=3, row=5)

suma = Button(frame, text ="+", width=2, command=lambda:clik("+"))
suma.grid(column=4, row=5)

fract= Button(frame,text="a/b",width=8,  command=lambda:clik ("Fraction"))
fract.grid(column=6,row=5)

porcen= Button(frame,text="%",width=8,command=lambda:clik("%"))
porcen.grid(column=6,row=4)

paren1=Button(frame,text="(",width=6,command=lambda:clik("("))
paren1.grid(column=1,row=6,columnspan=4)

paren2=Button(frame,text=")",width=6,command=lambda:clik(")"))
paren2.grid(column=1,row=6,columnspan=6)

igual = Button(frame, text ="=", width=22, command=lambda:O.operacion())
igual.grid(column=1, row=8, columnspan=5)

bClear = Button(frame, text ="Clear", width=22, command=lambda:O.clear())
bClear.grid(column=1, row=7, columnspan=5)

cm3_m3 = Button(frame, text ="cm3 a m3", width=8, command=lambda:V.cm3m3())
cm3_m3.grid(column=1, row=10)


m3_l = Button(frame, text ="m3 a l", width=8, command=lambda:V.m3l())
m3_l.grid(column=2, row=10)

l_gl = Button(frame, text ="l a gal", width=8, command=lambda:V.lgl())
l_gl.grid(column=3, row=10)

gl_cm3 = Button(frame, text ="gal a cm3", width=8, command=lambda:V.glcm3())
gl_cm3.grid(column=4, row=10)

pul_mm = Button(frame, text ="pul a mm", width=8, command=lambda:S.pulgadas())
pul_mm.grid(column=1, row=12)

mm_cm = Button(frame, text ="mm a cm", width=8, command=lambda:S.centimetros())
mm_cm.grid(column=2, row=12)

cm_m = Button(frame, text ="cm a m", width=8, command=lambda:S.metros())
cm_m.grid(column=3, row=12)

m_km = Button(frame, text ="m a km", width=8, command=lambda:S.kilometros())
m_km.grid(column=4, row=12)

pcl_atm = Button(frame, text ="Pa a atm", width=8, command=lambda:P.pascalatm())
pcl_atm.grid(column=1, row=13)

atm_torr = Button(frame, text ="atm a mmHg", width=10, command=lambda:P.atmtorr())
atm_torr.grid(column=2, row=13)

torr_pcl = Button(frame, text ="mmHg a Pa", width=8, command=lambda:P.torrpascal())
torr_pcl.grid(column=3, row=13)

gr_kil = Button(frame, text = "gr a kl", width=8, command=lambda:Pe.grkil())
gr_kil.grid(column=1, row=14)

kil_ton = Button(frame, text = "kil a ton", width=8, command=lambda:Pe.kilton())
kil_ton.grid(column=2, row=14)

ton_lib = Button(frame, text = "ton a lib", width=8, command=lambda:Pe.tonlib())
ton_lib.grid(column=3, row=14)


ventana.mainloop()